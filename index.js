const express = require('express')
const routes = require('./routes')
const path = require('path') // lee el filesystem
const bodyParser = require('body-parser');

// se crea una app de express
const app = express()

// Donde cargar archivos estaticos
app.use(express.static('public'))

// Habilitar Pug
app.set('view engine', 'pug')

// añadir la carpeta de las vistas
app.set('views', path.join(__dirname,'./views'))

// Habilitar bodyParser para leer datos del formulario
app.use(bodyParser.urlencoded({extended: true}))

app.use('/', routes())

app.listen(3001)