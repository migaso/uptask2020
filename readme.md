# UpTask 

## Instalar dependencias 
```bash
npm install
```

## Ejecutar desarrollo
```bash
npm run dev
```

## Instalar dependencias de desarrollo y agregarlas al proyecto 
```bash
npm install --save-dev "dependencia"
```
## Instalar dependencias del proyecto para producción
```bash
npm install --save "dependencia"
```
