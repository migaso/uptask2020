exports.proyectosHome = (req, res) => {
    res.render('index', {
        nombrePagina: 'Proyectos'     
    })
}

exports.formularioProyecto = (req,res) => {
    res.render('nuevoProyecto', {
        nombrePagina: 'Nuevo Proyecto'
    })
}

exports.nuevoProyecto = (req, res) =>{
    // console.log(req.body)
    // res.send('Enviaste el Formulario')
    const {nombre} = req.body
    let errors = []
    if(!nombre){
        errors.push({'texto':'agrega un nombre al proyecto'})
    }

    if(errors.length > 0){
        res.render('nuevoProyecto',{
            nombrePagina: 'Nuevo Proyecto',
            errors
        })
    } else {
       res.send('Gracias') 
    }
}